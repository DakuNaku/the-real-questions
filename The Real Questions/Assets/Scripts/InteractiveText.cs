﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractiveText : MonoBehaviour
{
    public Text QuestionTextbox;
    public Text AnswerTextG;
    public Text AnswerTextR;
    public Text GreenCounter;
    public Text RedCounter;

    public int GreenHit;
    public int RedHit;

    public ButtonClicked buttonClicked;

    // Start is called before the first frame update
    void Start()
    {
        GreenHit = 0;
        RedHit = 0;

        SetUpGame();
    }

    private void SetUpGame()
    {
        GreenCounter.text = GreenHit.ToString();
        RedCounter.text = RedHit.ToString();

    }

    // Update is called once per frame
    void Update()
    {

        GreenCounter.text = GreenHit.ToString();
        RedCounter.text = RedHit.ToString();

        if (GreenHit == 0 && RedHit == 0)
        {
            QuestionTextbox.text = ("Would you rather never play your favourite game again or never eat your favourite food again?");
            AnswerTextG.text = ("Never play my favourite game again.");
            AnswerTextR.text = ("Never eat my favourite food again.");
        }

        if (GreenHit == 1 && RedHit == 0)
        {
            QuestionTextbox.text = ("Would you rather jump out of a plane with no parachute or jump of a 150M bridge with no bungee cord?");
            AnswerTextG.text = ("I'll take the plane thx.");
            AnswerTextR.text = ("The bridge for me.");
        }
        
        if (GreenHit == 0 && RedHit == 1)
        {
            QuestionTextbox.text = ("Never shower/bathe again or never clean your teeth again?");
            AnswerTextG.text = ("I can't go without showering/batheing!");
            AnswerTextR.text = ("I rather have clean teeth!");
        }

        if (GreenHit == 1 && RedHit == 1)
        {
            QuestionTextbox.text = ("What would win a giant ant or 1000 ant sized krakens?");
            AnswerTextG.text = ("The giant ant.");
            AnswerTextR.text = ("1000 ant sized krakens");
        }

        if (GreenHit == 2 && RedHit == 0)
        {
            QuestionTextbox.text = ("Whats better Lynx or Play Boy?");
            AnswerTextG.text = ("Lynx.");
            AnswerTextR.text = ("Play Boy.");
        }

        if (GreenHit == 0 && RedHit == 2)
        {
            QuestionTextbox.text = ("Xbox or Ps?");
            AnswerTextG.text = ("Xbox.");
            AnswerTextR.text = ("Ps.");
        }

        if (GreenHit == 1 && RedHit == 2)
        {
            QuestionTextbox.text = ("Console or Pc?");
            AnswerTextG.text = ("Console.");
            AnswerTextR.text = ("Pc.");
        }

        if (GreenHit == 2 && RedHit == 1)
        {
            QuestionTextbox.text = ("Do you like Summer or Winter better?");
            AnswerTextG.text = ("Summer.");
            AnswerTextR.text = ("Winter.");
        }

        if (GreenHit == 1 && RedHit == 2)
        {
            QuestionTextbox.text = ("If you could live forever would you?");
            AnswerTextG.text = ("No way!");
            AnswerTextR.text = ("Of course i would!");
        }

        if (GreenHit == 2 && RedHit == 2)
        {
            QuestionTextbox.text = ("Only eat meat for the rest of your life or be a vegan for the rest of your life?");
            AnswerTextG.text = ("I can't live without meat");
            AnswerTextR.text = ("I'll be a vegan.");
        }

        if (GreenHit == 0 && RedHit == 3)
        {
            QuestionTextbox.text = ("Would you rather have 1 million dollars or have 500 thousand loyal friends?");
            AnswerTextG.text = ("Who needs friends when u have money.");
            AnswerTextR.text = ("Friends are everything to me.");
        }

        if (GreenHit == 3 && RedHit == 0)
        {
            QuestionTextbox.text = ("Are you sure this is a game?");
            AnswerTextG.text = ("Yes.");
            AnswerTextR.text = ("No.");
        }

        if (GreenHit == 1 && RedHit == 3)
        {
            QuestionTextbox.text = ("Is your life more important then someone else's?");
            AnswerTextG.text = ("It is.");
            AnswerTextR.text = ("It's not.");
        }

        if (GreenHit == 3 && RedHit == 1)
        {
            QuestionTextbox.text = ("Have you seen the TV show The 100?");
            AnswerTextG.text = ("Yes i have.");
            AnswerTextR.text = ("No i haven't");
        }

        if (GreenHit == 3 && RedHit == 2)
        {
            QuestionTextbox.text = ("Are you the only one in the room right now??");
            AnswerTextG.text = ("Yes?");
            AnswerTextR.text = ("No..");
        }

        if (GreenHit == 2 && RedHit == 3)
        {
            QuestionTextbox.text = ("Whats better chocolate or chips?");
            AnswerTextG.text = ("Chocolate!");
            AnswerTextR.text = ("Chips!");
        }

        if (GreenHit == 3 && RedHit == 3)
        {
            QuestionTextbox.text = ("Would you rather have your arms ripped off by an orc or have your legs shot off by a dwarf with a flintlock rife?");
            AnswerTextG.text = ("My arms ripped off by an orc.");
            AnswerTextR.text = ("My legs shot off by a dwarf with a flintlock rife.");
        }

        if (GreenHit == 4 && RedHit == 0)
        {
            QuestionTextbox.text = ("What's better Call Of Duty or Battlefield?");
            AnswerTextG.text = ("Cod!");
            AnswerTextR.text = ("Battlefield!");
        }

        if (GreenHit == 0 && RedHit == 4)
        {
            QuestionTextbox.text = ("Who would win King kong or an army of human sized rats 100 thousand strong?");
            AnswerTextG.text = ("King kong!");
            AnswerTextR.text = ("The army of rats!");
        }

        if (GreenHit == 4 && RedHit == 1)
        {
            QuestionTextbox.text = ("Apple juice or orange juice?");
            AnswerTextG.text = ("Apple juice.");
            AnswerTextR.text = ("Orange juice.");
        }

        if (GreenHit == 1 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you rather go to the beach or the mountians?");
            AnswerTextG.text = ("The beach!");
            AnswerTextR.text = ("The mountians!");
        }

        if (GreenHit == 4 && RedHit == 2)
        {
            QuestionTextbox.text = ("Are you sure....?");
            AnswerTextG.text = ("I think.");
            AnswerTextR.text = ("Not anymore!");
        }

        if (GreenHit == 2 && RedHit == 4)
        {
            QuestionTextbox.text = ("Are you Really who people think you are?");
            AnswerTextG.text = ("Yes.");
            AnswerTextR.text = ("No.");
        }

        if (GreenHit == 4 && RedHit == 3)
        {
            QuestionTextbox.text = ("Cats or Dogs?");
            AnswerTextG.text = ("Cats.");
            AnswerTextR.text = ("Dogs.");
        }

        if (GreenHit == 3 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you rather have the power to contorl time or the power to control space?");
            AnswerTextG.text = ("The power to control time.");
            AnswerTextR.text = ("The power to control space.");
        }

        if (GreenHit == 4 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you punch out your best mate for 1k?");
            AnswerTextG.text = ("You bet i would.");
            AnswerTextR.text = ("Nah not for only 1K.");
        }

        if (GreenHit == 5 && RedHit == 0)
        {
            QuestionTextbox.text = ("Would you rather fight 5, 20 year olds or fight 20, 5 yea olds?");
            AnswerTextG.text = ("The adults.");
            AnswerTextR.text = ("The kids.");
        }

        if (GreenHit == 0 && RedHit == 5)
        {
            QuestionTextbox.text = ("Have you ever fired a gun?");
            AnswerTextG.text = ("yes i have.");
            AnswerTextR.text = ("I cant say that i have.");
        }

        if (GreenHit == 5 && RedHit == 1)
        {
            QuestionTextbox.text = ("Have you eaten today?");
            AnswerTextG.text = ("Yes.");
            AnswerTextR.text = ("No.");
        }

        if (GreenHit == 1 && RedHit == 5)
        {
            QuestionTextbox.text = ("Do you Think ghosts are real?");
            AnswerTextG.text = ("Of course they are real!");
            AnswerTextR.text = ("No just no.");
        }

        if (GreenHit == 5 && RedHit == 2)
        {
            QuestionTextbox.text = ("Would you rather pull off all your finger nails with a fork or put tooth-picks under both of your big toe-nails and kick a brick wall as hard as you can?");
            AnswerTextG.text = ("I'd rather pull off my finger nails.");
            AnswerTextR.text = ("I'd rather put tooth-picks under my toe-nails and kick a wall.");
        }

        if (GreenHit == 2 && RedHit == 5)
        {
            QuestionTextbox.text = ("KFC or Hurngry Jacks?");
            AnswerTextG.text = ("KFC.");
            AnswerTextR.text = ("Hungry Jacks");
        }

        if (GreenHit == 5 && RedHit == 3)
        {
            QuestionTextbox.text = ("Would u rather live in the city or out in the country?");
            AnswerTextG.text = ("The city.");
            AnswerTextR.text = ("The country.");
        }

        if (GreenHit == 3 && RedHit == 5)
        {
            QuestionTextbox.text = ("Would you rather play or watch sport?");
            AnswerTextG.text = ("Play.");
            AnswerTextR.text = ("watch.");
        }

        if (GreenHit == 5 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you rather go to Romania or Libya?");
            AnswerTextG.text = ("Romania.");
            AnswerTextR.text = ("Libya");
        }

        if (GreenHit == 4 && RedHit == 5)
        {
            QuestionTextbox.text = ("Would you rather eat 1000 nuggets from a place of your choosing in one sitting or eat 13KG of steak with a marinade of your choosing in one sitting");
            AnswerTextG.text = ("The Nuggets.");
            AnswerTextR.text = ("The Steak.");
        }

        if (GreenHit == 5 && RedHit == 5)
        {
            QuestionTextbox.text = ("Would you rather eat a super sized baguette or 100 mini baguette's");
            AnswerTextG.text = ("Ill go big or go home.");
            AnswerTextR.text = ("The mini one's");
        }

        if (GreenHit == 6 && RedHit == 0)
        {
            QuestionTextbox.text = ("Would you run into a burning building to save someone? but if you did you have a 40% chance that you wont make it out alive.");
            AnswerTextG.text = ("Without a second throught!");
            AnswerTextR.text = ("40% chance of death? nah i'll leave saveing people to the pros.");
        }

        if (GreenHit == 0 && RedHit == 6)
        {
            QuestionTextbox.text = ("Do you know what FEF stands for?");
            AnswerTextG.text = ("How could i forget!");
            AnswerTextR.text = ("What's a FEF???");
        }

        if (GreenHit == 6 && RedHit == 1)
        {
            QuestionTextbox.text = ("If the toys from toy story were trying to kill you, would you fight back or run?");
            AnswerTextG.text = ("Fight back they are just toys.");
            AnswerTextR.text = ("Im running for sure.");
        }

        if (GreenHit == 1 && RedHit == 6)
        {
            QuestionTextbox.text = ("Whould you rather never eat anything again or never drink anything again?");
            AnswerTextG.text = ("Never eat again.");
            AnswerTextR.text = ("Never drink again.");
        }

        if (GreenHit == 6 && RedHit == 2)
        {
            QuestionTextbox.text = ("Who do you like better, Stitch or Nemo?");
            AnswerTextG.text = ("Stitch.");
            AnswerTextR.text = ("Nemo.");
        }

        if (GreenHit == 2 && RedHit == 6)
        {
            QuestionTextbox.text = ("Do you get hot easy or cold easy?");
            AnswerTextG.text = ("Hot easy.");
            AnswerTextR.text = ("Cold easy.");
        }

        if (GreenHit == 6 && RedHit == 3)
        {
            QuestionTextbox.text = ("Would you rather be somewhere else right now?");
            AnswerTextG.text = ("Yea 100% i would.");
            AnswerTextR.text = ("Nah there is no where id'd rather be.");
        }

        if (GreenHit == 3 && RedHit == 6)
        {
            QuestionTextbox.text = ("Would you rather travel to the year 3872 or the year 1174?");
            AnswerTextG.text = ("The year 3872.");
            AnswerTextR.text = ("The year 1174.");
        }

        if (GreenHit == 6 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you rather have to get into a boxing ring with The Rock for 10min or Swim 2 lengths of an olympic pool filled with non-lethal jellyfish?");
            AnswerTextG.text = ("Id rather get in the ring.");
            AnswerTextR.text = ("I'll take my chances in the pool thanks.");
        }

        if (GreenHit == 4 && RedHit == 6)
        {
            QuestionTextbox.text = ("Would you rather be a vampire or a werewolf?");
            AnswerTextG.text = ("A vampire.");
            AnswerTextR.text = ("A werewolf.");
        }

        if (GreenHit == 6 && RedHit == 5)
        {
            QuestionTextbox.text = ("What would win in a fight, a zombie Raccoon or a mummified Ring-tailed cat?");
            AnswerTextG.text = ("The zombie Raccoon.");
            AnswerTextR.text = ("The mummified Ring-tailed cat.");
        }

        if (GreenHit == 5 && RedHit == 6)
        {
            QuestionTextbox.text = ("Coffee or Tea?");
            AnswerTextG.text = ("Coffee.");
            AnswerTextR.text = ("Tea.");
        }

        if (GreenHit == 6 && RedHit == 6)
        {
            QuestionTextbox.text = ("What if this wasnt a game and your anwsers had real world consequences, would you still be happy with your answers?");
            AnswerTextG.text = ("Yes!");
            AnswerTextR.text = ("No!");
        }

        if (GreenHit == 7 && RedHit == 0)
        {
            QuestionTextbox.text = ("Would you rather be bitten by 2 crocodiles or be bitten by a great white shark?");
            AnswerTextG.text = ("2 Crocodiles.");
            AnswerTextR.text = ("A Great White Shark.");
        }

        if (GreenHit == 0 && RedHit == 7)
        {
            QuestionTextbox.text = ("Would you rather be a bird or a fish?");
            AnswerTextG.text = ("A bird.");
            AnswerTextR.text = ("A fish");
        }

        if (GreenHit == 7 && RedHit == 1)
        {
            QuestionTextbox.text = ("Footy or cricket?");
            AnswerTextG.text = ("Footy.");
            AnswerTextR.text = ("Cricket.");
        }

        if (GreenHit == 1 && RedHit == 7)
        {
            QuestionTextbox.text = ("Would you rather watch the Sunset or the Sunrise?");
            AnswerTextG.text = ("The Sunset.");
            AnswerTextR.text = ("The Sunrise.");
        }

        if (GreenHit == 7 && RedHit == 2)
        {
            QuestionTextbox.text = ("Would you rather be rich and homeless or be poor and have a home?");
            AnswerTextG.text = ("Rich and homeless.");
            AnswerTextR.text = ("Poor and have a home.");
        }

        if (GreenHit == 2 && RedHit == 7)
        {
            QuestionTextbox.text = ("Would you ever go skydiveing?");
            AnswerTextG.text = ("Yes.");
            AnswerTextR.text = ("No.");
        }

        if (GreenHit == 7 && RedHit == 3)
        {
            QuestionTextbox.text = ("Would you rather only listen to Classical music or only listen to Rock music?");
            AnswerTextG.text = ("Classical music.");
            AnswerTextR.text = ("Rock music.");
        }

        if (GreenHit == 3 && RedHit == 7)
        {
            QuestionTextbox.text = ("Hunting or Fishing?");
            AnswerTextG.text = ("Hunting.");
            AnswerTextR.text = ("Fishing.");
        }

        if (GreenHit == 7 && RedHit == 4)
        {
            QuestionTextbox.text = ("Would you rather focus on having a good career or focus on having a family?");
            AnswerTextG.text = ("A good career.");
            AnswerTextR.text = ("A family.");
        }

        if (GreenHit == 4 && RedHit == 7)
        {
            QuestionTextbox.text = ("Would you rather have all the land animals die or have all the sea animals die?");
            AnswerTextG.text = ("The land animals die.");
            AnswerTextR.text = ("The sea animals die.");
        }

        if (GreenHit == 7 && RedHit == 5)
        {
            QuestionTextbox.text = ("Would you rather know how you are going to die or when you are going to die?");
            AnswerTextG.text = ("How i'm going to die.");
            AnswerTextR.text = ("When i'm going to die.");
        }

        if (GreenHit == 5 && RedHit == 7)
        {
            QuestionTextbox.text = ("Nissan or Ford?");
            AnswerTextG.text = ("Nissan.");
            AnswerTextR.text = ("Ford.");
        }

        if (GreenHit == 7 && RedHit == 6)
        {
            QuestionTextbox.text = ("Would you sacrifice the one you love the most for the world or would you sacrifice the world for the one you love the most?");
            AnswerTextG.text = ("I'd sacrifice my loved one for the world.");
            AnswerTextR.text = ("I'd sacrifice the world for my loved one.");
        }

        if (GreenHit == 6 && RedHit == 7)
        {
            QuestionTextbox.text = ("Action or Adventure?");
            AnswerTextG.text = ("Action.");
            AnswerTextR.text = ("Adventure.");
        }

        if (GreenHit == 7 && RedHit == 7)
        {
            QuestionTextbox.text = ("Do you see yourself as an apex predator?");
            AnswerTextG.text = ("I do.");
            AnswerTextR.text = ("I don't.");
        }

        if (GreenHit == 8 || RedHit >= 8)
        {
            QuestionTextbox.text = ("There are 66 questions in total in this version of the game. Have you found and answered every question yet? *Please Note this is the last question in the session to go back to the main menu press escape (Clue: to find more question pick different answers)*");
            AnswerTextG.text = ("Yes and it took forever.");
            AnswerTextR.text = ("No i haven't.");
        }

        if (GreenHit == 9)
        {
            SceneManager.LoadScene(3);
        }
        //GreenCounter.text = GreenHit.ToString();
        //RedCounter.text = RedHit.ToString();
    }

}
