﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    //public void OpenURL(string url);

    //Loads game secene
    public void ButtonClicked(int buttonClicked)
    {
        if (buttonClicked == 1)
        {
            SceneManager.LoadScene(1);
        }
        else if (buttonClicked == 2)
        {
            SceneManager.LoadScene(2);
        }
        else if (buttonClicked == 3)
        {
            Application.Quit();
#if UNITY_EDITOR
#endif

        }
        else if (buttonClicked == 4)
        {
            SceneManager.LoadScene(0);
        }

        else if (buttonClicked == 5)
        {
            SceneManager.LoadScene(0);
        }

        else if (buttonClicked == 6)
        {
            SceneManager.LoadScene(4);
        }
    }

}
