﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAudio : MonoBehaviour
{
    public AudioSource buttonPressed;
    
    public void ButtonUsed()
    {
        buttonPressed.Play();
    }
 
}
