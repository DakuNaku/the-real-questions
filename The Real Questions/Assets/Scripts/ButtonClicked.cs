﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonClicked : MonoBehaviour
{
    public GameObject buttonGreen;
    public GameObject buttonRed;
    public InteractiveText interactiveText;
    public TriggerAudio triggerAudio;

    void Start()
    {
     
    }



    // Update is called once per frame
    void Update()
    {

        //Deactivates Button Base with button (Green) giving the illusion that the button has been pushed down
        if (Input.GetMouseButtonDown(0))
        {
            //A physics hit object to store info im going to get about where the ray hit
            RaycastHit hitinfo;

            //the distance of the ray the i'm using
            float distanceofRay = 100;

            //Makes the ray from ray cast the mouse loctaion (hopefully)
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // Raycast to where the mouse is
            Physics.Raycast(ray, out hitinfo, distanceofRay);

            if (hitinfo.collider.gameObject == buttonGreen)
            {
                //Debug.Log("it worked");
                buttonGreen.SetActive(false);
                //plays audio
                triggerAudio.ButtonUsed();
                //change the Counter
                interactiveText.GreenHit++;
            }

            else if (hitinfo.collider.gameObject == buttonRed)
            {
                //Debug.Log("it worked !!!!!!!!!!");
                buttonRed.SetActive(false);
                //plays audio
                triggerAudio.ButtonUsed();
                //change the counter
                interactiveText.RedHit++;
            }

        }
        //Reactivates Button Base with button (Red) giving the illusion that the button has come back up
        else if (Input.GetMouseButtonUp(0))
        {
            //Debug.Log("please work!!!!");
            buttonRed.SetActive(true);
            buttonGreen.SetActive(true);
        }

        //interactiveText.GreenCounter.text = interactiveText.GreenHit.ToString();
        //interactiveText.RedCounter.text = interactiveText.RedHit.ToString();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }
}